# email-configrules-extraction

A script to match certain rules from .ini config files with a provided email file using regular expressions.
Rules in .ini file can be of two types: header and body. Rules will be matching headers and body (payload) of an email file respectively.
Script is supposed be ran from a command line and needs to be provided with three arguments: 
* a path to rule config file;
* path(s) to email file(s); 
* a path to logging config file (optional argument: if not specified, the default config will be used).
`python confrules_extraction.py [path-to-config-file] [path(s)-to-email-file(s)] -l [path-to-logging-config-file]`
The output consists of a list of email filenames that have been matched and scored, a list of body and header rules that were matched, a score for each email file processed and a category that matches the score. 
