import datetime
import logging
import logging.config
import sqlite3
from dataclasses import asdict, fields
from string import Template

from email_scanning.models.email_file_model import EmailFileModel
from email_scanning.models.rule_model import RuleModel, HeaderRuleModel, BodyRuleModel
from email_scanning.models.rules_preference_model import RulesPreferenceModel


logging.config.fileConfig(fname="../logging.conf")
logger = logging.getLogger("dbLogger")

PYTHON_TO_SQL = {
    int: "integer",
    float: "real",
    str: "text",
    datetime.datetime: "text"
}
DECLARE_COLUMN = Template("$name $type NOT NULL")
DECLARE_PK = Template("PRIMARY KEY ($pks)")
DECLARE_FK = Template("""FOREIGN KEY ($fks) REFERENCES $table($fields)
                         ON DELETE CASCADE""")
DELETE_FROM_TABLE = Template("""DELETE FROM $table;""")
DROP_TABLE = Template("""DROP TABLE IF EXISTS $table;""")
CREATE_TABLE = Template("""CREATE TABLE IF NOT EXISTS $name (
                           $statements
                           );""")
INSERT_VALUES = Template("""INSERT INTO $table($columns) VALUES($values)""")
INSERT_OR_REPLACE = Template("""INSERT OR REPLACE INTO $table($columns) VALUES($values);""")
UPDATE_RULES_PREFERENCE = """INSERT INTO rules_preferences(key, value) VALUES(:key,:value)
                             ON CONFLICT(key) DO UPDATE
                             SET key = :key, value = :value
                             WHERE key = key"""
SELECT_ENTIRE_ROW = Template("""SELECT * FROM $table;""")
SELECT_BY_PK = Template("""SELECT * FROM $table
                           WHERE $pk_field = ?""")

LIST_TABLES = """SELECT name FROM sqlite_master WHERE type='table';"""
SELECT_HEADER_RULES = """SELECT * FROM
                             (SELECT rules.name, rules.version, score, rule_type, header_rules.header, header_rules.body_re
                              FROM rules
                              INNER JOIN header_rules
                              ON rules.name = header_rules.name AND rules.version = header_rules.version)
                         WHERE version = (SELECT MAX(version) FROM rules)
                         GROUP BY name;"""

SELECT_BODY_RULES = """SELECT * FROM
                           (SELECT rules.name, rules.version, score, rule_type, body_rules.body_re
                            FROM rules
                            INNER JOIN body_rules
                            ON rules.name = body_rules.name AND rules.version = body_rules.version)
                       WHERE version = (SELECT MAX(version) FROM rules)
                       GROUP BY name;"""

QUERY_MAX_VERSION = """SELECT * FROM rules
                       WHERE version=(SELECT MAX(version) FROM rules)
                       AND name=?;"""

SELECT_TYPED_RULE = Template("""SELECT * FROM ${ruletype}_rules
                                WHERE name=?""")



def check_version(cursor, rule_name):
    """
    Checks if there are previous versions of the rule present in the
    database.

    Parameters:
        cursor(sqlite3.Cursor): Database connection cursor.
        rule_name(str): Name of the rule whose previous version needs
                        to be checked.

    Returns:
        int: Version to be set for this particular rule.
    """

    version = 1
    try:
        cursor.execute(QUERY_MAX_VERSION, (rule_name,))
        max_version = cursor.fetchone()
    except sqlite3.OperationalError as e:
        logger.error(e)
        return
    if max_version:
        version = dict(max_version)['version'] + 1
    return version


def create_tables(cursor, *args):
    """
    Creates tables in the database.

    Parameters:
        cursor(sqlite3.Cursor): Database connection cursor.
        args(list): a list of models from which the tables are created.
    """

    for model in args:
        cursor.execute(make_table(model))


def insert_rule(cursor, record):
    """
    Inserts rule record into the database.

    Parameters:
        cursor(sqlite3.Cursor): Database connection cursor.
        record(RuleModel): A RuleModel instance that respresents a
                           rule database record.
    """

    insert_data = asdict(record)
    placeholders = ",".join(["?"] * len(insert_data))
    statement = INSERT_VALUES.substitute(table='rules',
                                         columns=", ".join(insert_data.keys()),
                                         values=placeholders)
    cursor.execute(statement, tuple(insert_data.values()))


def insert_typed_rule(cursor, record, typed_record):
    """
    Insert record rule of a particular type (header or body) into the
    database, as well as the associated basic rule record.

    Parameters:
        cursor(sqlite3.Cursor): Database connection cursor.
        record(RuleModel): A RuleModel instance that respresents the
                           rule record.
        types_record(BodyRuleModel or HeaderRuleModel): Either a HeaderRuleModel
                                                        or a BodyRuleModel instance that represents
                                                        a rule database record of a particular type.
    """

    insert_data = asdict(typed_record)
    placeholders = ",".join(["?"] * len(insert_data))
    statement = INSERT_VALUES.substitute(table=typed_record.meta.table_name,
                                         columns=", ".join(insert_data.keys()),
                                         values=placeholders)
    insert_rule(cursor, record)
    cursor.execute(statement, tuple(insert_data.values()))


def insert_rules_preference(cursor, record):
    """
    Inserts rules preference record into the database.

    Parameters:
        cursor(sqlite3.Cursor): Database connection cursor.
        record(RulesPreferenceModel): a model object that represents a
                                      rules preference database record.
    """

    insert_data = asdict(record)
    placeholders = ",".join(["?"] * len(insert_data))
    statement = INSERT_VALUES.substitute(table='rules_preferences',
                                         columns=", ".join(insert_data.keys()),
                                         values=placeholders)
    cursor.execute(statement, tuple(insert_data.values()))


def update_rules_preference(cursor, record):
    """
    Updates rules preference database record.

    Parameters:
        cursor(sqlite3.Cursor): Database connection cursor.
        record(RulesPreferenceModel): a model object that contains the data
                                      with which the corresponding is updated.
    """

    update_data = asdict(record)
    values = {'key': update_data['key'], 'value': update_data['value']}
    cursor.execute(UPDATE_RULES_PREFERENCE, values)


def insert_email_file(cursor, record):
    """
    Inserts email file record into the database.

    Parameters:
        cursor(sqlite3.Cursor): Database connection cursor.
        record(EmailFileModel): a model object that represents an
                                email file database record.
    """

    insert_data = asdict(record)
    placeholders = ",".join(["?"] * len(insert_data))
    statement = INSERT_OR_REPLACE.substitute(table='email_files',
                                             columns=", ".join(insert_data.keys()),
                                             values=placeholders)
    cursor.execute(statement, tuple(insert_data.values()))


def load_email_file(cursor, email_data):
    """
    Initializes EmailFileModel instances from provided email data
    and inserts them into the database.

    Parameters:
        cursor(sqlite3.Cursor): Database connection cursor.
        email_data(dict): A dictionary containing a scanned email
                          message and corresponding scan results.
    """

    for email, scan_results in email_data.items():
        email_file = EmailFileModel(email=email,
                                    score=scan_results['score'],
                                    rules=scan_results['rules'])
    insert_email_file(cursor, email_file)


def load_rules(cursor, rule_data):
    """
    Initializes appropriate models from a provided rule data and
    inserts ruleset into the database.

    Parameters:
        cursor(sqlite3.Cursor): Database connection cursor.
        rule_data(dict): A ruleset data that has been extracted from
                         an .ini config file.
    """

    rules = []
    typed_rules = []
    for rule in rule_data:
        for name, value in rule.items():
            value['version'] = check_version(cursor, name)
            rule_type = value['rule_type']
            rules.append(RuleModel(name=name,
                                   version=value['version'],
                                   score=value['score'],
                                   rule_type=rule_type))
            del value['score']
            del value['rule_type']
            typed_rules.append(RuleModel.types[rule_type](name=name, **value))
    for rule, typed_rule in zip(rules, typed_rules):
        insert_typed_rule(cursor, rule, typed_rule)


def compare_rules(cursor, rule):
    """
    Checks if there is a need to bump up the rule version
    on update by comparing the newly provided data with an already
    existing rule record with a corresponding name from a database.

    Parameters:
        cursor(sqlite3.Cursor): Database connection cursor.
        rule(dict): A newly provided rule data.
    """

    for name, value in rule.items():
        try:
            cursor.execute(QUERY_MAX_VERSION, (name,))
            latest_record = cursor.fetchone()
            if not latest_record:
                return True
            cursor.execute(
                SELECT_TYPED_RULE.substitute(ruletype=latest_record['rule_type']),
                (name,)
            )
            typed_record = cursor.fetchone()
            typed_record = dict(typed_record)
            typed_record['score'] = latest_record['score']
            for key, value in typed_record.items():
                if key not in ('version', 'name'):
                    if not str(value) == rule[name].get(key):
                        return True
        except sqlite3.OperationalError as e:
            logger.error(e)
            return
    return False


def update_rules(cursor, rule_data):
    """
    Updates rule database records with a provided data.
    If there are any new rules, they are inserted, if the existing ones
    are modified, they are updated and their version increments. Otherwise,
    this function ignores already existing records in which no changes have been
    detected.

    Parameters:
        cursor(sqlite3.Cursor): Database connection cursor.
        rule_data(dict): Data extracted from an .ini config
                         file with which the database is to be
                         updated.
    """

    rules = []
    typed_rules = []
    for rule in rule_data:
        if compare_rules(cursor, rule):
            for name, value in rule.items():
                value['version'] = check_version(cursor, name)
                rule_type = value['rule_type']
                rules.append(RuleModel(name=name,
                                       version=value['version'],
                                       score=value['score'],
                                       rule_type=rule_type))
                del value['score']
                del value['rule_type']
                typed_rules.append(RuleModel.types[rule_type](name=name, **value))
    for rule, typed_rule in zip(rules, typed_rules):
        insert_typed_rule(cursor, rule, typed_rule)


def load_rules_preferences(cursor, rules_preference_data):
    """
    Initializes RulesPreferencesModel objects from rules preference data
    extracted from config file and inserts corresponding records into the
    database.

    Parameters:
        cursor(sqlite3.Cursor): Database connection cursor.
        rules_preference_data(dict): Rules preference data that has been
                                     extracted from config file.
    """

    rules_preferences = []
    for key, value in rules_preference_data.items():
            rules_preferences.append(RulesPreferenceModel(key=key,
                                                          value=str(value)))
    for rules_preference in rules_preferences:
        insert_rules_preference(cursor, rules_preference)


def update_rules_preferences(cursor, rules_preference_data):
    """
    Initializes RulesPreferenceModel objects from a newly provided
    rules preference data extracted from config file and updates rules
    preferences database records with it.

    Parameters:
        cursor(sqlite3.Cursor): Database connection cursor.
        rules_preference_data(dict): Rules preference data that has been
                                     extracted from config file.
    """

    rules_preferences = []
    for key, value in rules_preference_data.items():
        rules_preferences.append(RulesPreferenceModel(key=key,
                                                      value=str(value)))
    for rules_preference in rules_preferences:
        update_rules_preference(cursor, rules_preference)


def get_ruleset(cursor):
    """
    Reads rule records from a database.

    Parameters:
        cursor(sqlite3.Cursor): Database connection cursor.

    Returns:
        list: Ruleset records from a database represented as a
              list of dictionaries.
    """

    rules = []
    try:
        cursor.execute(SELECT_HEADER_RULES)
        rules.extend([dict(rule) for rule in cursor.fetchall()])
        header_rules = cursor.fetchall()
        rules.extend([dict(rule) for rule in header_rules])
        cursor.execute(SELECT_BODY_RULES)
        body_rules = cursor.fetchall()
        rules.extend([dict(rule) for rule in body_rules])
    except sqlite3.OperationalError as e:
        logger.error(e)
        return
    return rules


def get_rules_preferences(cursor):
    """
    Reads rules preferences records from a database.

    Parameters:
        cursor(sqlite3.Cursor): Database connection cursor.

    Returns:
        list: Rules preferences records from a database represented
              as a list of dictionaries.
    """

    extracted_rules_preferences = []
    try:
        cursor.execute(
            SELECT_ENTIRE_ROW.substitute(table='rules_preferences')
        )
        rules_preferences = cursor.fetchall()
        for rules_preference in rules_preferences:
            extracted_rules_preferences.append(dict(rules_preference))
    except sqlite3.OperationalError as e:
        logger.error(e)
        return
    return extracted_rules_preferences


def get_email_file(cursor, email_data):
    """
    Reads a specific email file record from a database.

    Paramerers:
        cursor(sqlite3.Cursor): Database connection cursor.
        email_data(EmailFile): Email object to extract primary
                               key value from.

    Returns:
        dict: Dictionary representing email record from a database.
    """

    email_file = EmailFileModel(email=email_data,
                                score=None,
                                rule_hits=None)
    try:
        cursor.execute(
            SELECT_BY_PK.substitute(table='email_files',
                                    pk_field='email_id'),
            (email_file.email_id,)
        )
        email_record = cursor.fetchone()
    except sqlite3.OperationalError as e:
        logger.error(e)
        return
    return dict(email_record)


def delete_database(cursor):
    """
    Deletes database by deleting all records, dropping all tables
    and subsequently removing the database file altogether.

    Parameters:
        cursor(sqlite3.Cursor): Database connection cursor.
    """

    try:
        cursor.execute(LIST_TABLES)
        tables = [name[0] for name in cursor.fetchall()]
        for table in tables:
            cursor.execute(DELETE_FROM_TABLE.substitute(table=table))
            cursor.execute(DROP_TABLE.substitute(table=table))
    except sqlite3.OpreationalError as e:
        logger.error(e)
        return


def make_table(model):
    """
    Constructs an SQL statement for table creation based on a specific model.

    Parameters:
        model(dataclasses.dataclass): A model that serves as a blueprint for a database table.

    Returns:
        str: An SQL statement for table creation.
    """

    statements = []
    for field in fields(model):
        statements.append(
            DECLARE_COLUMN.substitute(name=field.name,
                                      type=PYTHON_TO_SQL[field.type])
        )
    statements.append(
        DECLARE_PK.substitute(pks=", ".join(model.meta.primary_keys))
        )
    if model.meta.foreign_keys:
        statements.append(
            DECLARE_FK.substitute(fks=", ".join(model.meta.primary_keys),
                                  table=model.meta.to_table,
                                  fields=", ".join(model.meta.to_fields))
        )
    return CREATE_TABLE.substitute(name=model.meta.table_name,
                                   statements=",\n".join(statements))
