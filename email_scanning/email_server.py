import datetime
import json
import logging
import smtpd
import smtplib
import sqlite3
from dataclasses import asdict
from string import Template

from email_scanning import exceptions
from email_scanning import db_manager
from email_scanning.email_file import EmailFile
from email_scanning.models.email_file_model import EmailFileModel
from email_scanning.rule_factory import rule_factory
from email_scanning.ruleset_config import RulesetConfig
from email_scanning.rule_matcher import RuleMatcher
from email_scanning.transaction import Transaction


RELAY_IP = '127.0.0.1'
RELAY_PORT = 1075
SCAN_HEADER = Template("""Rule hits - $rule_hits; Email score - $score; scan time - $scan_time""")
WARNING_BANNER = """\n--- Be careful. The contents of this message are suspicious. ---"""
QUARANTINE_DIR = '../quarantined'

class ActionsMixin:
    """
    A mixin class that implements action methods that are called
    after an incoming email has been scanned depending on a scan verdict.
    """

    def soft_quarantine(self, incoming_email):
        """
        Saves email message to the quarantine directory, while relaying
        it to another server.

        Parameters:
            incoming_email(EmailFile): an incoming email that the action applies to.

        Returns:
            str: A '250 Ok' response in RFC 5321 format.
        """

        filename = 'email_{timestamp}'.format(timestamp=datetime.datetime.now())
        path = '{quarantine_dir}/{filename}'.format(
                                                quarantine_dir=QUARANTINE_DIR,
                                                filename=filename
                                             )       
        incoming_email.save(path)                   
        status = self.relay(incoming_email)
        return status               
                                                                
    def hard_quarantine(self, incoming_email):
        """
        Saves email message to a quarantine directory while rejecting it.

        Parameters:
            incoming_email(EmailFile): an incoming email that the action applies to.

        Returns:
            str: A '554 Rejected' response in RFC 5321 format.
        """

        filename = 'email_{timestamp}'.format(timestamp=datetime.datetime.now())
        path = '{quarantine_dir}/{filename}'.format(
                                                quarantine_dir=QUARANTINE_DIR,
                                                filename=filename
                                             )
        incoming_email.save(path)
        status = self.reject(incoming_email)
        return status

    def relay(self, incoming_email):
        """
        Relays email message to another SMTP server.

        Parameters:
            incoming_email(EmailFile): an incoming email that the action applies to.

        Returns:
            str: A '250 Ok' response in RFC 5321 format.
        """

        smtp = smtplib.SMTP(RELAY_IP, RELAY_PORT)
        smtp.send_message(incoming_email.message)
        return '250 Ok'

    def reject(self, incoming_email):
        """
        Rejects incoming email and prevents it from being forwarded any further.

        Parameters:
            incoming_email(EmailFile): an incoming email that the action applies to.

        Returns:
            str: A '554 Rejected' response in RFC 5321 format.
        """

        return '554 Rejected'

    def caution(self, incoming_email):
        """
        Adds a warning banner to an email message while relaying it to another 
        SMTP server.

        Parameters:
            incoming_email(EmailFile): an incoming email that the action applies to.

        Returns:
            str: A '554 Rejected' response in RFC 5321 format.
        """

        modified_parts = []
        for part in incoming_email.body:
            modified_parts.append(part + WARNING_BANNER.encode())
        incoming_email.modify_payload(modified_parts)
        status = self.relay(incoming_email)
        return status


class EmailScanningSMTPServer(smtpd.SMTPServer, ActionsMixin):
    """
    A class that represents email scanning SMTP server.

    Attributes:
        connection(DBConnection: A database connection.
        output_path(str): Path to an output file.
    """ 

    def __init__(self, local_addr, remote_addr, connection, output_path):
        super().__init__(local_addr, remote_addr)
        self.connection = connection
        self.output_path = output_path

    def scan(self, email):
        """
        Scans an incoming email by matching rules from the database to it and
        calculating an email score.

        Parameters:
            email(EmailFile): an incoming email to be scanned.

        Returns:
            dict: Scan results of an email message.
            EmailFileModel: Rule record that contains fields necessary for adding
                            a custom header and writing the scan output to a JSON file.
            dict: Mapping of verdict names and corresponding actions.
        """

        try:
            with Transaction(self.connection) as t:
                rule_values = db_manager.get_ruleset(t)
                rules_preferences = db_manager.get_rules_preferences(t)
        except sqlite3.OperationalError as e:
            logging.error(e)
            return
        
        for rule in rule_values:
            del rule['version']
        rules = rule_factory(rule_values)
        if not rules:
            return

        ruleset_config = {}
        for rules_preference in rules_preferences:
            ruleset_config[rules_preference['key']] = rules_preference['value']
        rc = RulesetConfig(**ruleset_config)

        rm = RuleMatcher(rules, rc)
        match_results = rm.match_rules(email)
    
        with Transaction(self.connection) as t:
           db_manager.load_email_file(t, {email: match_results})

        email_record = EmailFileModel(email=email,
                                      score=match_results['score'],
                                      rules=match_results['rules'])
            
        return match_results, email_record, rc.actions

    def process_message(self, peer, mailfrom, rcpttos, data, 
                        mail_options=None, rcpt_options=None):
        """
        Scans an incoming email, adds a custom header to it, takes an approptiate
        action depending on scan results and writes an output to the file if the output
        file path is provided.

        Returns:
            str: A response in RFC 5321 format depending on the action taken.
        """

        incoming_email = EmailFile(data)
        verdict, email_record, actions_mapping = self.scan(incoming_email)
        incoming_email.message["X-Scan-Results"] = SCAN_HEADER.substitute(
                                                                   rule_hits=email_record.rule_hits,
                                                                   score=email_record.score,
                                                                   scan_time=email_record.scan_time
        
                                                               )
        action = getattr(self, actions_mapping[verdict['class']])
        status = action(incoming_email)

        if self.output_path:
            self.write_output(email_record, verdict)

        return status

    def write_output(self, email_record, verdict):
        """
        Writes scan results to the file.

        Parameters:
            email_record(EmailFileModel): A scanned email record containing
                                          necessary fields for the output.
            verdict(dict): A dictionary containing scan results. Is used to extract
                           scan verdict from it.
        """

        scanned_email = asdict(email_record)
        scanned_email.update({'verdict': verdict['class']})
        try:
            with open(self.output_path, 'a') as output_file:
                output_file.write(json.dumps(
                                           scanned_email, 
                                           indent=4, 
                                           sort_keys=True, 
                                           default=str)
                )
        except (IsADirectoryError, FileNotFoundError) as e:
            logging.error(exceptions.InvalidOutputFilePath(self.output_path))
            return
