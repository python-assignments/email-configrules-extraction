import argparse
import asyncore
import configparser
import logging
import logging.config
import os
import sys
import sqlite3

from email_scanning import exceptions
from email_scanning.extract_config import extract_config
from email_scanning.email_file import EmailFile
from email_scanning.rule_factory import rule_factory
from email_scanning.rule_matcher import RuleMatcher
from email_scanning.ruleset_config import RulesetConfig
from email_scanning import db_manager
from email_scanning.email_server import EmailScanningSMTPServer
from email_scanning.db_connection import DBConnection
from email_scanning.models.email_file_model import EmailFileModel
from email_scanning.models.rule_model import RuleModel, HeaderRuleModel, BodyRuleModel
from email_scanning.models.rules_preference_model import RulesPreferenceModel
from email_scanning.transaction import Transaction


IP_ADDR = '127.0.0.1'
PORT = 1025
DB_ACTIONS = ['create', 'update', 'read', 'delete']


def create_database(db_connection, config):
    """
    Creates database by creating necessary tables and populating it
    with ruleset and ruleset configrutions specified in a config file.

    Parameters:
        db_connection(sqlite3.Connection): A database connection.
        config(configparser.ConfigParser): A config file containig ruleset
                                           and ruleset configurations.
    """

    ruleset_config, rule_values = extract_config(config)
    with Transaction(db_connection) as t:
        db_manager.create_tables(t,
                                 EmailFileModel,
                                 RulesPreferenceModel,
                                 RuleModel,
                                 HeaderRuleModel,
                                 BodyRuleModel)
        db_manager.load_rules(t, rule_values)
        db_manager.load_rules_preferences(t, ruleset_config)


def update_database(db_connection, config):
    """
    Updates an existing database with ruleset and ruleset configurations
    specified in a config file. Fails to do so if database doesn't alrady
    exist.

    Parameters:
        db_connection(sqlite3.Connection): A database connection.
        config(configparser.ConfigParser): A config file containing ruleset
                                           and ruleset preferences.
    """

    ruleset_config, rule_values = extract_config(config)
    try:
        with Transaction(db_connection) as t:
            db_manager.update_rules(t, rule_values)
            db_manager.update_rules_preferences(t, ruleset_config)
    except sqlite3.OperationalError:
        logging.error(exceptions.DBNotFound(args.database))
        return


def read_database(db_connection):
    """
    Extracts rules and ruleset configrations from a database and displays them
    to a terminal. Fails to do so if the database doesn't already exist.

    Parameters:
        db_connection(sqlite3.Connection): A database connection.
    """

    try:
        with Transaction(db_connection) as t:
            rules = db_manager.get_ruleset(t)
            rules_preferences = db_manager.get_rules_preferences(t)
    except sqlite3.OperationalError:
        logging.error(exceptions.DBNotFound(args.database))
        return
    print("### RULESET ###")
    for rule in rules:
        for key, value in rule.items():
            print("{key}: {value}".format(key=key,
                                          value=value))
        print('\n')
    print('\n')
    print("### RULES PREFERENCES ###")
    for rules_preference in rules_preferences:
        for key, value in rules_preference.items():
            print("{key}: {value}".format(key=key,
                                          value=value))


def delete_database(db_connection, database):
    """
    Deletes database by deleting all the records, dropping tables and
    removing database file. Fails to do so if the database doesn't already
    exist.

    Parameters:
        db_connection(sqlite3.Connection): A database connection.
        database_path(str): A path to database file.
    """

    with Transaction(db_connection) as t:
        db_manager.delete_database(t)
    os.remove(database)


def load_config(args):
    config = configparser.ConfigParser()
    config.read(args.config)
    db_connection = DBConnection(args.database_path).connection

    if args.action == 'create':
        create_database(db_connection, config)
    elif args.action == 'update':
        update_database(db_connection, config)
    elif args.action == 'read':
        read_database(db_connection)
    elif args.action == 'delete':
        delete_database(db_connection, args.database_path)
 

def run_server(args):
    db_connection = DBConnection(args.database_path).connection

    server = EmailScanningSMTPServer((IP_ADDR, PORT), None, db_connection, args.output_file)

    try:
        asyncore.loop()
    except KeyboardInterrupt:
        server.close()


def main():

    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    parser.add_argument('-l', '--logging_config', help="""A path to a logging config .ini file. Specify
                                                          this argument to provide the script with you own
                                                          logging configurations.""")
    parser.add_argument('-db-path', '--database-path', help="""A path to the database file. Provide this
                                                               argument to specify the location of your database.""")

    db_parser = subparsers.add_parser('db',
                                      help="""Enables user to either create a database from a config
                                              file by loading ruleset and preferences into a database,
                                              to update an already existing database with new ruleset or
                                              preferences, to display contents of databse into the terminal,
                                              or to delete an already existing database.""")
    db_parser.add_argument('action',
                           choices=DB_ACTIONS,
                           help="""Provide this argument to specify how the script should interact
                                   with database. Note that read, delete and update options can only
                                   be used if the database has already been created.""")
    db_parser.add_argument('config', 
                           type=str,
                           help="""A path to the config file.""")
    db_parser.set_defaults(func=load_config)

    run_server_parser = subparsers.add_parser('run',
                                              help="""Sets up and runs an SMTP server on port 1025 that performs scanning
                                                    on incoming emails and takes an appropriate action depending on scan
                                                    results.""")
    run_server_parser.add_argument('-o',
                                   '--output-file',
                                   help="""A path to the output file.
                                         Enables the script to write output into JSON file.""")
    run_server_parser.set_defaults(func=run_server)

    args, unknown = parser.parse_known_args()

    args.logging_config = args.logging_config or '../logging.conf'

    logging.config.fileConfig(fname=args.logging_config)
    logging.getLogger(__name__)

    args.database_path = args.database_path or '../email_scanning.db'

    args.func(args)


if __name__ == "__main__":
    main()
