class RuleMatcher:
    """
    A class that manages matching rule objects with an email file.

    Attributes:
        rules (a list or tuple containing Rule objects): rules to be matched.
        ruleset_config (RulesetConfig object): an object that contains ruleset configurations
                                               necessary for email classification.
    """

    def __init__(self, rules, ruleset_config):
        self.rules = rules
        self.ruleset_config = ruleset_config

    def match_rules(self, email):
        """
        Matches rules on an email message.

        Parameters:
            email (EmailFile): email message to be matched against a list of rules.

        Returns:
            dict: matching results that contain data about which rules have specifically
                  been matched, what score an email message had received and a label (verdict)
                  assigned to an email based on its score.
        """

        matched = {'rules': [], 'score': 0.}
        for rule in self.rules:
            if rule.is_a_match(email):
                matched['rules'].append(rule)
        for rule in self.rules:
            if rule in matched['rules']:
                matched['score'] += rule.score
        round(matched['score'], 1)
        matched['class'] = self.ruleset_config.classify_email(matched['score'])
        return matched
