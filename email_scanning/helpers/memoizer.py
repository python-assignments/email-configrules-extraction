def memoizer(f):
    memoized = {}
    def memo(*args):
        key = args
        if key not in memoized:
            memoized[key] = f(*args)
        return memoized[key]
    return memo
