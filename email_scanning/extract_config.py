import logging
import sys

from email_scanning import exceptions


def parse_verdicts(section_name, section_value, ruleset_config):
    """
    Parses and properly structured verdicts configurations.
    All verdict names must be listed under the "verdicts" value of the main section.
    A verdict section must be declared after the main configuration section.

    Parameters:
        section_name(str): Name of the verdict section. The proper format
                           is "verdict_%verdict_name%".
        section_value(dict): Configurations of the verdict section.
        ruleset_config(dict): Main section of ruleset configurations.
    """

    if not ruleset_config:
        logging.error(exceptions.InvalidRulesetConfigurationOrder(section_name))
        sys.exit(1)
    verdict_name = section_name.lstrip('verdict_')
    if not ruleset_config.get('verdicts') or verdict_name not in ruleset_config['verdicts']:
        logging.error(exceptions.InvalidVerdictSection(verdict_name))
        sys.exit(1)
    else:
        try:
            section_value['range'] = float(section_value['range'])
        except KeyError:
            section_value['range'] = None
    ruleset_config['verdicts'][verdict_name] = section_value


def parse_ruleset_config(section_value):
    """
    Parses main section of ruleset configurations (titled ruleset_config) and
    properly structures it.
    This is the section where all verdicts must be listed, otherwise the
    configuration will be invalid.

    Parameters:
        section_value(dict): contents of the main section.

    Returns:
        dict: properly structured ruleset_config contents.
    """

    ruleset_config = section_value
    if 'verdicts' in section_value.keys():
        ruleset_config['verdicts'] = dict((verdict, None)
                                     for verdict in ruleset_config['verdicts'].split(', '))
    return ruleset_config


def extract_config(config):
    """
    Extracts rules and ruleset configurations from config file.

    Parameters:
        config(configparser.ConfigParser): an .ini config file to extract
                                           values (ruleset_config and rules) from.

    Returns:
        ruleset_config(dict): A dictionary containing ruleset configurations.
        rule_values(list): A list of dictionaries containing extracted rules.
    """

    rule_values = []
    ruleset_config = None
    for section_name in config.sections():
        section_value = dict(config.items(section_name))
        if section_name == 'ruleset_config':
            ruleset_config = parse_ruleset_config(section_value)
            continue
        if section_name.startswith('verdict_'):
            parse_verdicts(section_name, section_value, ruleset_config)
            continue
        if not section_value.get('rule_type'):
            logging.error(exceptions.AbsentRuleType(section_name))
        else:
            rule_values.append({section_name: section_value})
    if ruleset_config is None:
        logging.error(exceptions.AbsentRulesetConfig())
        sys.exit(1)
    return ruleset_config, rule_values
