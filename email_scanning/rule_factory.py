import logging

from email_scanning import exceptions
from email_scanning.rule_unit import HeaderRule, BodyRule


def rule_factory(values):
    """
    Creates a list of HeaderRule or BodyRule objects based on provided values.

    Parameters:
        values(list): A list of mappings that contain ruleset data.

    Returns:
        list: A list of HeaderRule and BodyRule objects.
    """

    rules_mapping = {
        'header': HeaderRule,
        'body': BodyRule
    }
    rules = []
    for rule in values:
        rule_type = rule.get('rule_type')
        if rule_type in rules_mapping.keys():
            del rule['rule_type']
            rules.append(rules_mapping[rule_type](**rule))
        else:
            logging.error(exceptions.InvalidRuleType(rule_type))
    return rules
