import smtplib

class EmailScannerException(Exception):
    """
    Base exception class for project-related exceptions.
    """

class InvalidConfigInput(EmailScannerException):
    """
    Raised when a function or method recieves invalid input from a config file.
    """


class InvalidEmailInput(EmailScannerException):
    """
    Raised when a function or method receives invalid email input.
    """


class InvalidRuleType(InvalidConfigInput):
    """
    Raised when rule has an invalid type.
    """

    def __init__(self, ruletype):
        self.ruletype = ruletype
        self.message = "Type {ruletype} is not an allowed type.".format(ruletype=self.ruletype)
        super().__init__(self.message)


class AbsentRuleType(InvalidConfigInput):
    """
    Raised when rule does not have a type value.
    """

    def __init__(self):
        self.message = "Rule must have a type value."
        super().__init__(self.message)


class AbsentRulesetConfig(InvalidConfigInput):
    """
    Raised when the ruleset_config section is not specified.
    """

    def __init__(self):
        self.message = "Ruleset configuration must be provided."
        super().__init__(self.message)

class InvalidRulesetConfigurationOrder(InvalidConfigInput):
    """
    Raised when ruleset configurations declaration order is invalid.
    For example, if the configuration of the specific scanning verdict is
    declared before the main configuration section.
    """

    def __init__(self, section_name):
        self.message = "{section_name} must be declared after the main " \
                       "ruleset_config section.".format(section_name=section_name)
        super().__init__(self.message)


class InvalidVerdictSection(InvalidConfigInput):
    """
    Raised when the verdict is not specified in the main ruleset_config
    section, but has its own configuration section.
    """

    def __init__(self, verdict):
        self.message = "Unrecognized verdict {verdict}. Please, specify " \
                       "it in the main ruleset_config section.".format(verdict=verdict)
        super().__init__(self.message)


class EmailNotFound(InvalidEmailInput):
    """
    Raised when a path to an email file is invalid.
    """

    def __init__(self, emailpath):
        self.emailpath = emailpath
        self.message = "{path} is not a valid path to email file.".format(path=self.emailpath)
        super().__init__(self.message)

class DBException(EmailScannerException):
    """
    Raised when the error related to database functionality occurs.
    """

class DBNotFound(DBException):
    """
    Raised when database does not exist.
    """

    def __init__(self, db_path):
        self.db_path = db_path
        self.message = "Database {path} does not exist.".format(path=self.db_path)
        super().__init__(self.message)

class InvalidOutputFilePath(EmailScannerException):
    """
    Raised when the path to an output file is for some reason invalid (e.g. is a directory
    or does not exist.
    """

    def __init__(self, output_path):
        self.output_path = output_path
        self.message = "Path to the output file {path} is invalid.".format(
                                                                        path=self.output_path
                                                                    )
        super().__init__(self.message)
