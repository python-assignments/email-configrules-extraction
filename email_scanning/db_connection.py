import logging
import sqlite3

from email_scanning.helpers.memoizer import memoizer


logging.config.fileConfig(fname='../logging.conf')
logger = logging.getLogger('dbLogger')


@memoizer
class DBConnection:
    """
    A class that represents a connection to database.

    Attributes:
        connection (sqlite3.Connection object): a connection to database with specified db_name.
    """

    def __init__(self, db_name):
        self.connection = None
        try:
            self.connection = sqlite3.connect(db_name)
            self.connection.row_factory = sqlite3.Row
        except sqlite3.OperationalError as e:
            logger.error(e)

    def __del__(self):
        if self.connection:
            self.connection.close()

