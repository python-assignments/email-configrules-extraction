import re


class Rule:
    """
    A base class for a rule unit to be matched.

    Attributes:
        allowed_props (tuple): Names of attributes that are allowed to be initialized from a dict.
        name (str): Name of the rule (title of a section in a config file, a key in a
                    dictionary etc).
    """

    def __init__(self, name, score):
        self.name = name
        self.score = float(score)

    def is_a_match(self):
        """
        A method that should be implemented by any Rule class subclasses. Indicates whether a
        particular rule matched against an email message.
        """

        pass


class HeaderRule(Rule):
    """
    A class that represents rule object of the header type.

    Attributes:
        ruletype (str): Type of the rule object (in this case, header).
        name (str): Name of the rule (title of a section in a config file, a key in a
                    dictionary etc).
        header (str): Header of an email message that contains the value to be matched by a
                      regular expression.
        body_re (str): Regular expression that matches against a specified header value.
    """

    ruletype = 'header'

    def __init__(self, name, header, body_re, score):
        super().__init__(name, score)
        self.header = header
        self.body_re = re.compile(body_re)
        self.matches = []

    def is_a_match(self, email):
        """
        Matches a rule object agains email message header.

        Parameters:
            email (EmailFile): An email message to be matched against the rule.

        Returns:
            bool: A boolean value that indicates whether an email message header
                  is a match to the rule.
        """

        value = email.message[self.header]
        if not value:
            return False
        matches = re.findall(self.body_re, value)
        return bool(matches)


class BodyRule(Rule):
    """
    A class that represents rule object of a body type.

    Attributes:
        ruletype (str): Type of the rule object (in this case, body).
        name (str): Name of the rule (title of a section in a config file, a key in a
                    dictionary etc).
        body_re (str): Regular expression that matches against the body of an email message.
    """

    ruletype = 'body'

    def __init__(self, name, body_re, score):
        super().__init__(name, score)
        self.body_re = re.compile(body_re.encode())

    def is_a_match(self, email):
        """
        Matches a rule object against an email message body.

        Parameters:
            email (EmailFile): An email message to be against the rule.

        Returns:
            bool: A boolean value that indicates whether an emaill message
                  body was a match to the rule.
        """

        matches = []
        for part in email.body:
            matches.extend(re.findall(self.body_re, part))
        return bool(matches)
