import email
import email.generator


class EmailFile:
    """
    A class responsible for encapsulating email file objects.

    Attributes:
        data (bytes): Incoming email message received by SMTP server.
        body (str): A payload of the email message.
    """

    def __init__(self, data: bytes):
        msg = email.message_from_bytes(data)
        self.message = msg
 
    @property
    def body(self):
        """
        Initializes body attribute of an object by decoding the payload of the
        message.

        Returns:
            list: A list of decoded email message parts.
        """

        if self.message.is_multipart():
            return [part.get_payload(decode=True) for part in self.message.get_payload()]
        return [self.message.get_payload(decode=True)]

    def modify_payload(self, values: list):
        """
        Changes the value message payload by replacing it with the specified
        values.

        Parameters:
            values(list): List of values with which the existing payload is
                          replaced.
        """

        if self.message.is_multipart():
            for index, part in enumerate(self.message.get_payload()):
                part.set_payload(values[index])
        else:
            self.message.set_payload(values[0])

    def save(self, filename: str):
        """
        Writes email message to the file while modyfing a payload and
        replacing it with decoded body attribute beforehand.

        Parameters:
            filename(str): a path to the file for a message to be written to.
        """

        self.modify_payload(self.body)
        with open(filename, 'wb') as f:
            generator = email.generator.BytesGenerator(f)
            generator.flatten(self.message)
