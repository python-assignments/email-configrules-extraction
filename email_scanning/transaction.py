import logging
import logging.config

logging.config.fileConfig(fname='../logging.conf')
logger = logging.getLogger('dbLogger')


class Transaction:
    """
    A context manager class that handles database transactions.

    Attributes:
        connection (sqlite3.Connection): database connection object.
    """

    def __init__(self, connection):
        self.connection = connection

    def __enter__(self):
        self.cursor = self.connection.cursor()
        return self.cursor

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if exc_type:
            logger.error(exc_value)
            self.connection.rollback()
        self.connection.commit()
        self.cursor.close()
