from dataclasses import dataclass, field
from typing import List


@dataclass
class MetaModel:
    """
    A class that represents a collection of metadata fields necessary for DB table creation.

    Attributes:
        table_name(str): name of the table.
        primary_keys(list): list of field to be used as primary keys of the table.
        foreign_keys(list): list of foreign keys.
        to_fields(list): list of fields that are referenced by the table foreign keys.
        to_table(str): a name of the table referenced by this table's foreign keys.
    """

    table_name: str
    primary_keys: List[str]
    foreign_keys: List[str] = field(default_factory=list)
    to_fields: List[str] = field(default_factory=list)
    to_table: str = None
