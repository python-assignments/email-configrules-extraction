from dataclasses import dataclass
from typing import ClassVar

from email_scanning.models.meta_model import MetaModel


@dataclass
class RulesPreferenceModel:
    """
    A class that represents rules preference data model.

    Attributes:
        meta (MetaModel): pseudofield that adds metadata to the model.
        key (str): name of a rules preference.
        value (str): contents of a rules preference.
    """

    meta: ClassVar = MetaModel(table_name='rules_preferences',
                               primary_keys=['key'])

    key: str
    value: str

