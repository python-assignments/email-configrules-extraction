from dataclasses import dataclass
from typing import ClassVar

from email_scanning.models.meta_model import MetaModel


@dataclass
class HeaderRuleModel:
    """
    A class that represents header rule data model.

    Attributes:
        meta (MetaModel): pseudofield that adds metadata to the model.
        name (str): name of the rule.
        version (int): version of the rule.
        header (str): name of the header to be matched against.
        body_re (str): value of regular expression to be matched against specified header.
    """

    meta: ClassVar = MetaModel(table_name='header_rules',
                               primary_keys=['name', 'version'],
                               foreign_keys=['name', 'version'],
                               to_fields=['name', 'version'],
                               to_table='rules')
    name: str
    version: int
    header: str
    body_re: str


@dataclass
class BodyRuleModel:
    """
    A class that represents body rule data model.

    Attributes:
        meta (MetaModel): pseudofield that adds metadata to the model.
        name (str): name of the rule.
        version (int): version of the rule.
        body_re (str): value of regular expression to be matched against specified header.
    """

    meta = ClassVar = MetaModel(table_name='body_rules',
                                primary_keys=['name', 'version'],
                                foreign_keys=['name', 'version'],
                                to_fields=['name', 'version'],
                                to_table='rules')
    name: str
    version: int
    body_re: str


@dataclass
class RuleModel:
    """
    A class that represents rule data model.

    Attributes:
        meta (MetaModel): pseudofield that adds metadata to the model.
        name (str): name of the rule.
        version (int): version of the rule.
        score (float): score value associated with the rule.
        rule_type(str): type of the rule.
    """

    meta: ClassVar = MetaModel(table_name='rules',
                               primary_keys=['name', 'version'])
    types: ClassVar = {
                'header': HeaderRuleModel,
                'body': BodyRuleModel
            }
    name: str
    version: int
    score: float
    rule_type: str

