import hashlib
from dataclasses import dataclass, field, InitVar
from datetime import datetime
from typing import ClassVar, List

from email_scanning.email_file import EmailFile
from email_scanning.models.meta_model import MetaModel


@dataclass
class EmailFileModel:
    """
    A class that represents email file data model (including the file's scan resulst).

    Attributes:
        meta (MetaModel): pseudofield that adds metadata to the model.
        email (EmailFile): pseudofield that stores EmailFile object used to calculate
                           email_id field.
        rules (list): pseudofield that stores a lits of matched Rule objects and is used to create
                      to construct rule_hits field.
        score (float): field that stores score of the email file as the result of scanning.
        rule_hits (str): field that stores rule hits for the email file as the result of scanning.
        email_id (int): field that stores unique file's id (inode number).
        scan_time (str): field that stores the latest scan time.
    """

    meta: ClassVar = MetaModel(table_name='email_files',
                               primary_keys=['email_id'])
    email: InitVar[EmailFile]
    rules: InitVar[List]

    score: float
    rule_hits: str = None
    email_id: str = None
    scan_time: datetime = field(default_factory=datetime.utcnow)

    def __post_init__(self, email, rules):
        self.email_id = self.construct_email_id(email)
        self.rule_hits = self.construct_rule_hits(rules)

    @staticmethod
    def construct_email_id(email):
        email_hash = hashlib.md5()
        for part in email.body:
            email_hash.update(part)
        return email_hash.hexdigest()

    @staticmethod
    def construct_rule_hits(rules):
        rule_hits = ", ".join(["{0}: {1}".format(rule.name, rule.score)
                              for rule in rules])
        return rule_hits
