import ast


class RulesetConfig:
    """
    A class that represents a ruleset configuration.

    Attributes:
        verdicts(dict): A mapping of verdict names, their ranges and actions to be taken if this
                        verdict is returned as a result of email scanning.
    """

    def __init__(self, verdicts):
        verdicts = ast.literal_eval(verdicts)
        self.actions = self.assign_actions(
                           ((verdict_name, verdict_value.get('action'))
                           for verdict_name, verdict_value in verdicts.items())
                       )
        self.ranges = self.calculate_ranges(
                          ((verdict_name, verdict_value['range'])
                          for verdict_name, verdict_value in verdicts.items())
                      )

    @staticmethod
    def assign_actions(actions):
        """
        Structures the actions attribute of an object. It maps the verdict name to the
        corresponding action or assigns reject action if no actions had been provided.

        Parameters:
            actions(Generator[tuple, None, None]): a generator containing pairs of verdict
                                                   names and corresponding actions as specified
                                                   in the config file.

        Returns:
            dict: A mapping of verdict names and corresponding actions.
        """

        return {verdict: action or 'reject' for verdict, action in actions}

    @staticmethod
    def calculate_ranges(ranges):
        """
        Maps verdict names to their respective numeric intervals. Each interval
        is represented as a tuple.

        Parameters:
            ranges(Generator[tuple, None, None]): a generator containing pairs of verdict
                                                  names and corresponding ranges as specified
                                                  in the config file.

        Returns:
            dict: A mapping of verdict names and their correpsonding intervals represented
                  as tuples.
        """

        ranges = sorted(
                     ranges,
                     key=lambda x: (x[1] is None, x[1])
                 )
        verdicts, points = zip(*list(ranges))
        l = [float('-inf'), *points[:-1], float('inf')]
        return dict(zip(verdicts, zip(l[:-1], l[1:])))

    def classify_email(self, score):
        """
        Assigns a verdict to a scanned email message based on the scanning score.

        Parameters:
            score(float): A score calculated in the process of scanning.

        Returns:
            str: A verdict assigned to an email based on scanning results.
        """

        for verdict, points in self.ranges.items():
            if points[0] <= score < points[1]:
                return verdict
